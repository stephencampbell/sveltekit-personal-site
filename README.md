# personal-site

## Scripts
| Command               | Description
|-----------------------|----------------------------------------------|
| npm run dev           | Run dev server with HMR                      |
| npm run dev -- --open | Run dev server with HMR and open browser     |
| npm run build         | Build site assets (output to build/)         |
| npm run start         | Start server for previewing production build |
| npm run deploy        | Build and deploy to App Engine               |
| node build            | Run for production                           |
