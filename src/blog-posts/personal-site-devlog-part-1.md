---
{
  "handle": "personal-site-devlog-part-1",
  "title": "Personal Site Development Log: Part 1",
  "date": "2021-03-25",
  "summary": "Requirements, design and build log for my personal website."
}
---

## The idea
I've wanted a personal site for a long time now, and I even used to have one
based on self-hosted WordPress. Since then, I've worked as a web developer
and become more interested in the idea of creating optimised, accessible
websites.

So the question, then, is how did I go about it?

## The requirements
Before we can look at design or build, we first need to identify the
requirements:
  - A blog with rich articles
  - High page speed
  - Good developer experience
  - Easy (and ideally free) to host

## The tooling
Given these basic requirements, I started to think about tooling.
I prefer to work in the context of JavaScript frameworks, so I
knew I'd be using something like that.

### Candidates
Most of my experience is with Vue, but I wanted to know about the
other options out there.

#### [Alpine](https://github.com/alpinejs/alpine)

Alpine is a minimal JavaScript framework focused on keeping file size down.
I set up an Alpine app to give it a try. Syntactically I wasn't a huge fan
of this framework, and even though it's lightweight, it isn't necessarily
performant. [This article](https://javascript.plainenglish.io/javascript-frameworks-performance-comparison-2020-cd881ac21fce)
summarises the performance of various JavaScript frameworks on a range of
metrics and found Alpine to actually be slower than a lot of the larger
frameworks tested.

#### [Svelte](https://svelte.dev/)

Svelte doesn't work like traditional frameworks. Instead of importing
a JavaScript library and using it at runtime, Svelte adds a build
step that compiles your app into reactive code. Svelte ended up being the
framework I settled on for building the site. The syntax especially is
extremely clean as a result of Svelte having the freedom to operate
outside of just being a package imported at runtime.

### Code examples
So let's look at some code examples. This is the same component built with
the frameworks listed above. It is worth noting that some best practices
(like labels for input fields) have left out of these code samples for
the sake of brevity.

#### Vue
```html
<template>
  <div>
    <button @click="addItem">Add</button>
    <ul>
      <li :key="index" v-for="(todoItem, index) in todoItems">
        <input type="checkbox" v-model="todoItem.checked">
        {{ todoItem.text }}
      </li>
    </ul>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        todoItems: []
      }
    },
    methods: {
      addItem() {
        this.todoItems.push({
          text: 'test',
          checked: false
        })
      }
    }
  }
</script>
```

#### Alpine
```html
<div x-data="todoList()">
  <button @click="addItem()">Add</button>
  <ul>
    <li x-for="item in items">
      <input x-model="item.checked" type="checkbox">
      <span x-text="item.text"></span>
    </li>
  </ul>
</div>

<script>
  function todoList() {
    return {
      items: [],
      addItem() {
        this.items.push({
          text: 'test',
          checked: false
        })
      }
    }
  }
</script>
```

#### Svelte
```svelte
<script>
  let todoItems = []
  
  const addItem = () => {
    todoItems = [...todoItems, {
      text: 'test',
      checked: false
    }]
  }
</script>

<div>
  <button on:click={addItem}>Add</button>
  <ul>
    {#each todoItems as todoItem}
      <li>
        <input type="checkbox" bind:checked={todoItem.checked}>
        {todoItem.text}
      </li>
    {/each}
  </ul>
</div>
```

### Choosing the tooling
After settling on Svelte, it was just a case of looking into other tools
I wanted to use in the development of the site. Having used
[Nuxt.js](https://nuxtjs.org/) before for Vue, I wanted to find
something similar for Svelte that provided serverside rendering,
handled a lot of the boilerplate and so on.

Initially I came across [Sapper](https://sapper.svelte.dev/), a framework
by the Svelte team that uses Svelte. It worked great but after I found
[it was announced](https://svelte.dev/blog/whats-the-deal-with-sveltekit)
that Sapper was never to reach a version 1.0, I decided to look elsewhere.

My first port of call was [Awesome Svelte](https://github.com/TheComputerM/awesome-svelte#frameworks). 
From there, I tried a few Svelte-based frameworks, including
[Elder.js](https://elderguide.com/tech/elderjs/) before I came across
a tool called [Routify](https://routify.dev/). I built an early version of the
website with Routify before hearing that 
[SvelteKit had reached public beta](https://svelte.dev/blog/sveltekit-beta)
and decided that this would be the tooling I would base the site on.

## The design
The design of this site drew inspiration from a few different sources.
I wanted something playful and casual, with a retro feel while also being
relatively modern and easy to implement in a lightweight way.

### Colour scheme
The various shades of coffee and cream colours on this site were picked as
they are warm and easy on the eyes. Very high contrast has been used for
shadows, borders and fonts to provide a comic book feel, which helps the
site to feel more casual.

### Shadows
The hard drop shadows shown around page content as well as the borders were
inspired by the buttons on [Florence By Mills](https://florencebymills.com/),
a website I used to work on.

### Fonts
The title font in use on this site is hosted by Adobe fonts, but was a font
I came across on the [Rama Works](https://rama.works/) website. When I saw
this font it seemed like a perfect fit for the intended feeling of my
personal site.

For the body font and monospace fonts, I have used Fira Sans and Fira Mono
respectively. These are beautiful fonts from Mozilla's Fira family.

### Other touches
The wavy pattern seen at the top of the page was inspired by a very similar
feature on the [CSS Tricks](https://css-tricks.com/) website.
I found a [PHP REPL](https://replit.com/@ebimammadi/generate-clip-path-wave)
that included code to generate these patterns and used that to generate a 
cosine wave-based clip path. I've included the code here for convenience.

```php
<?php
// based on a stackOverFlow question 
// https://stackoverflow.com/questions/61157568/create-clip-path-wave-css-edges

$width_px = 1000;
$height_px = 200;
$offset = 100;
$amplitude = 30;
$frequency = 1.3;
$units = $frequency * $width_px / 100;

$clipPathString = 'clip-path: polygon(100% 0%, 0% 0% ';

for ($i = 0; $i <= 100; $i++){
  $val = $offset + $amplitude * cos($i / $units);
  $val = round ($val, 1)/$height_px * 100;
  $clipPathString .= ', ' . $i . '% ' . $val . '%';
}
$clipPathString .= ');';

echo $clipPathString;

echo '<br><br><br>';

$html_element = '<div style="width:1000px;height:400px; '
.'background-color:blue;' . $clipPathString . '"><div>';

echo $html_element;
```

The background pattern used in code blocks and title blocks comes from
[Hero Patterns](http://www.heropatterns.com/).

## The build
Building the site with the early public beta of SvelteKit had its fair
share of challenges. I implemented Markdown parsing with
[Marked](https://github.com/markedjs/marked). Importing it directly with
an ESM import presented the [require is not defined](https://github.com/sveltejs/kit/issues/655)
error. After much hassle, I eventually worked out that importing the
CJS version of the module directly fixes the issue.

Initially I started the build using [Bulma](https://bulma.io/) with 
[PurgeCSS](https://github.com/FullHuman/purgecss) but later changed
my mind in favour of writing custom CSS to keep the feature set and
file size down as much as possible. The styling on this site is very
simple so it wasn't too much of a pain to reinvent the wheel on
this one.

The rest of the build went smoothly with performance testing throughout to
ensure that anything I added didn't damage the page speed. I configured
the font imports on Adobe Fonts to reduce their file sizes as much as
possible.

If you'd like to see the code for this project, you can
[visit my GitLab](https://gitlab.com/stephencampbell/personal-site).

## Hosting
This site is hosted for free on Google Cloud's App Engine. Configuration
with SvelteKit was fairly minor and mostly just worked out the box.
The scripts I added to `package.json` are as follows:
```json
{
  "scripts": {
    "start": "node build",
    "deploy": "npm run build && gcloud app deploy --quiet"
  }
}
```

## Summary
Thanks for reading this article! This project was a fun and interesting way
to learn SvelteKit and get my personal site built. Along the way I learned
a few of the quirks to look out for and ended up with a result I'm pleased
with. Do a Lighthouse test on this page and look at the results!
