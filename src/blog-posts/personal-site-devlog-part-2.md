---
{
  "handle": "personal-site-devlog-part-2",
  "title": "Personal Site Development Log: Part 2",
  "date": "2021-04-10",
  "summary": "Further work on the personal site."
}
---

## Next steps
Since the first development log, I've learned a lot more about SvelteKit
by directly speaking with members of the development team on their
Discord server.

As a result of this, I've refined the backend logic for loading blog
a bit and cleaned it up.

Alongside this, I've extensively cleaned up the frontend code,
including a rework of the responsive CSS framework and DOM
structure.

Previously, I had used Bulma's flex-based grid system. The main issue
with this is that it relies on having quite a few DOM elements in place
to handle each group of columns and each individual column. The reworked
CSS uses CSS grid instead, which allows me to drop these DOM elements
entirely.

Here you can see a comparison of the old DOM structure and the new.

Before:
```html
<Page>
  <div class="container">
    <div class="columns is-multiline">
      <div class="column is-12">
        <div class="card">
          <p>Hello world</p>
        </div>
      </div>
    </div>
  </div>
</Page>
```

After:
```html
<Page>
  <section>
    <article>
      <p>Hello world</p>
    </article>
  </section>
</Page>
```

These two code snippets achieve exactly the same thing, with grid
row spacing, responsive design (if needed - in this instance
both are set to full-width columns) and styling.

The code for the new responsive grid is as follows
(variables replaced with values for simplicity's sake):
```less
.container
  display: grid
  grid-gap: 1rem

  &.responsive
    grid-template-columns: repeat(auto-fit, minmax(260px, 1fr))
```

That's it! To make a container with responsive columns, it needs
the responsive class and it'll automatically wrap to a single line on
mobile.

## Styling changes
I have also revamped the navigation - has a hamburger icon on mobile
and upper case text using the title font. Pages and navigation are
also animated. These animations are fully powered by Svelte transitions.

The site's colour scheme has also been changed. Instead of creams and
browns, it's now mostly grayscale. This is a bit less polarising but
the styling of the site itself remains relatively unchanged.
