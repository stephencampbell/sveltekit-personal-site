---
{
  "handle": "personal-site-devlog-part-3",
  "title": "Personal Site Development Log: Part 3",
  "date": "2021-07-29",
  "summary": "Porting the site to Tailwind and a full redesign."
}
---

## The new site
Still based on the same SvelteKit codebase (albeit updated to a newer version of SvelteKit), I decided
to port the site to a CSS framework. I decided on Tailwind after using it for a few personal projects and being
exposed to it at work. Tailwind offers some challenges (to create a "component" as you would in Bootstrap or Bulma,
you're best off abstracting your code out into a new Svelte component so styles aren't repeated) but provides
a level of flexibility in terms of styling each page individually that component-based CSS frameworks struggle
to match.

With this in mind, I thought it would be best for pages to have different feelings to them, which can help
to differentiate them and highlight the content on the page. The new site design is also more modern than
the design that preceded it, though still has unconventional features (icon-based navigation is typically not
used as mobile users are unable to see the tooltips, as an example).

I'm still not completely happy with the new site, but I think it's fairly clean and features the information
I want it to at the moment. The blog post page itself is still due an update and the home page is lacking
content.

## Future updates
On top of the aforementioned design changes, something I've wanted to handle for a while are photo albums and
photo categorisation. I'm thinking that hosting the images on GCP or AWS instead of Flickr would provide the
flexibility not only to categorise them a bit, but also would allow me to serve them in more efficient formats
and as such, achieve a higher Lighthouse score on the site.

I'd also like to create a new page to highlight my music, but I think it's worth getting the parts of the site
I already have right before moving on to expanding its scope.
