import fs from 'fs'
import path from 'path'
import marked from 'marked/lib/marked.js'
import hljs from 'highlight.js'

// Register additional highlight.js languages
import hljsDefineSvelte from 'highlightjs-svelte'
hljsDefineSvelte(hljs)

// Get all post front matter
const getAllPosts = () => {
  return fs.readdirSync('src/blog-posts')
    // Parse front matter for each post
    .map(fileName => {
      const post = fs.readFileSync(path.resolve('src/blog-posts', fileName), 'utf-8')
      return parseMarkdown(post).matter
    })
    // Sort by date in descending order
    .sort((a, b) => new Date(b.date) - new Date(a.date))
}

// Get single post by handle
const getPost = handle => {
  const post = fs.readFileSync(path.resolve('src/blog-posts', `${handle}.md`), 'utf-8')
  return parseMarkdown(post)
}

// Markdown parsing utility function
const parseMarkdown = (data, delimiter = '---') => {
  // Syntax highlighting
  marked.setOptions({
    highlight: function(code, lang) {
      return hljs.highlight(code, { language: lang }).value
    }
  })

  // Split out front matter
  const [,matter,...markdown] = data.split(delimiter)

  // Return front matter and parsed markdown
  return {
    matter: JSON.parse(matter),
    markdown: marked(markdown.join(delimiter))
  }
}

// Controller
export function get({ params }) {
  const { handle } = params

  // /blog/api/all
  if(handle === 'all') return { body: { posts: getAllPosts() } }

  // /blog/api/{handle}
  return { body: { post: getPost(handle) } }
}
